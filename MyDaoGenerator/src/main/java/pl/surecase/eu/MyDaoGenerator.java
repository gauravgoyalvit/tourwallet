package pl.surecase.eu;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

public class MyDaoGenerator {

 public static void main(String args[]) throws Exception {
  Schema schema = new Schema(2, "com.recording.models");
     schema.enableKeepSectionsByDefault();

     // for testing purpose
  Entity upload = schema.addEntity("Upload");
  upload.addIdProperty();
  upload.addStringProperty("text");


// Trip Table
     Entity trip  = schema.addEntity("Trip");
     trip.addIdProperty();


     // Location related data table
     Entity coordinates  = schema.addEntity("Coordinates");
     coordinates.addIdProperty();
     coordinates.addDoubleProperty("latitude");
     coordinates.addDoubleProperty("longitude");
     coordinates.addFloatProperty("accuracy");
     coordinates.addDoubleProperty("altitude");
     coordinates.addFloatProperty("bearing");
     coordinates.addLongProperty("realtimeElipsedNanos");
     coordinates.addStringProperty("provider");
     coordinates.addFloatProperty("speed");
     coordinates.addLongProperty("time");
     Property tripId = coordinates.addLongProperty("tripId").getProperty();


     Entity pics  = schema.addEntity("Pics");
     pics.addIdProperty();
     Property tripId_= pics.addLongProperty("pictripid").getProperty();
     pics.addStringProperty("picname");
    pics.addStringProperty("tag");
     pics.addStringProperty("premises");
     pics.addStringProperty("subthoroughfare");
     pics.addStringProperty("thoroughfare");
     pics.addStringProperty("sublocality");
     pics.addStringProperty("locality");
     pics.addStringProperty("subadminarea");
     pics.addStringProperty("adminarea");
     pics.addStringProperty("countryname");
     pics.addDateProperty("photoDate");


     Entity newTrip = schema.addEntity("NewTrip");
     newTrip.addIdProperty();
     newTrip.addStringProperty("name");
     newTrip.addStringProperty("photoname");
     newTrip.addBooleanProperty("tripon");
     newTrip.addStringProperty("locations");
     newTrip.addDateProperty("startingtime");
     newTrip.addDateProperty("endtime");


     coordinates.addToOne(newTrip, tripId);
     newTrip.addToMany(coordinates, tripId, "trip");

     pics.addToOne(newTrip, tripId_);

  //  ../../
  new DaoGenerator().generateAll(schema, "app/src/main/java-gen");
 }
}