package mingo.jobs;

/**
 * Created by mak on 11/27/14.
 */
public class JobPriority {
    public static final int PHOTO_SYNC = 1;
    public static final int DElAY_IN_PHOTO_SYNC = 2000;

    // photos code
    public static final int ALLPHOTO = 0;
    public static final int COUNTRYLEVELPHOTO = 1;
    public static final int ADMINAREALEVELPHOTO = 2;
    public static final int LOCALITYAREALEVELPHOTO = 3;
}
