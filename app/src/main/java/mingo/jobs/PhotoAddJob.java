package mingo.jobs;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import com.path.android.jobqueue.Params;
import com.recording.models.Coordinates;
import com.recording.models.NewTrip;
import com.recording.models.Pics;
import com.recording.models.Trip;

import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeoutException;

import mingo.TripApplication;
import mingo.managers.CoordinatesManager;
import mingo.managers.NewTripManager;
import mingo.managers.PicsManager;
import mingo.managers.TripManager;

/**
 * Created by wael on 12/2/14.
 */
public class PhotoAddJob extends JobWithContext {
    public static final int PRIORITY = JobPriority.PHOTO_SYNC;
    private String imagePath;
    private Address address;
    private Date date_;


    public PhotoAddJob(String imagePath) {
        // This job requires network connectivity,
        // and should be persisted in case the application exits before job is completed.
        super(new Params(PRIORITY).requireNetwork().persist().delayInMs(JobPriority.DElAY_IN_PHOTO_SYNC));
        this.imagePath = imagePath;
        Date currentDate = Calendar.getInstance().getTime();
        java.text.SimpleDateFormat simpleDateFormat = new java.text.SimpleDateFormat("dd/MM/yyyy");
        String formattedCurrentDate = simpleDateFormat.format(currentDate);
        try{
            date_ = simpleDateFormat.parse(formattedCurrentDate);
            Log.d("tag",date_.toString());
        }
        catch (Exception e){
            ;
        }
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        Location loc = TripApplication.getInstance().getLocation();
        Context context = TripApplication.getInstance().getApplicationContext();
        Geocoder gCoder = new Geocoder(context);
        try {
            List<Address> addresses = gCoder.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);

           //   List<Address> addresses = gCoder.getFromLocation(27.1750151,78.0421552, 1);
            if (addresses != null && addresses.size() > 0) {
                address = addresses.get(0);
            }
        } catch (IOException e) {
            if(e.getCause() instanceof IOException)
            {
                TripApplication.getInstance().getJobManager()
                        .addJob(new PhotoAddJob(imagePath));
                return;
            }
            Log.d("tag",e.toString());
            ;
        }
        if(address!=null) {
            Pics pics = new Pics();
            NewTrip trip = NewTripManager.isAnyTripRunningAlready(context);
            pics.setNewTrip(trip);
            pics.setAdminarea(address.getAdminArea()); // karmatka
            pics.setCountryname("india");
            pics.setLocality(address.getLocality());// bangalore
            pics.setPicname(imagePath);
            pics.setPictripid(trip.getId());
            pics.setPremises(address.getPremises());
            pics.setSubadminarea(address.getSubAdminArea()); // null or bangalore
            pics.setSublocality(address.getSubLocality());
            pics.setSubthoroughfare(address.getSubThoroughfare());
            pics.setThoroughfare(address.getThoroughfare());
            pics.setPhotoDate(date_);
            PicsManager.insertOrReplace(context, pics);
            List<Pics> list = PicsManager.loadAll(context);
            Log.d("tag", list.toString());
            // insert data into db here.
        }
        else {
            // in case address was not retrieved
            Pics pics = new Pics();
            NewTrip trip = NewTripManager.isAnyTripRunningAlready(context);
            pics.setNewTrip(trip);
            pics.setPicname(imagePath);
            pics.setPictripid(trip.getId());
            pics.setPhotoDate(date_);
            PicsManager.insertOrReplace(context, pics);
            List<Pics> list = PicsManager.loadAll(context);
            Log.d("tag", list.toString());
        }

//        PicsManager.getHighestLevel(context,id);
//        PicsManager.returnPics(context,4);
        // Job logic goes here. In this example, the network call to post to Twitter is done here.

    }

    @Override
    protected void onCancel() {
        // Job has exceeded retry attempts or shouldReRunOnThrowable() has returned false.

    }
}
