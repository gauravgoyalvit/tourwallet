package mingo.jobs;

import android.content.Context;
import android.content.SharedPreferences;

import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;

import java.io.IOException;


/**
 * Created by wael on 12/1/14.
 */
public abstract class JobWithContext extends Job {
    Context context;

    public JobWithContext() {
        // default priority is pull
        super(new Params(JobPriority.DElAY_IN_PHOTO_SYNC).requireNetwork().persist());
    }

    public JobWithContext(Params params) {
        super(params);
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        // An error occurred in onRun.
        // Return value determines whether this job should retry running (true) or abort (false).
//        if (throwable instanceof RetrofitError) {
//
//        }
        return true;
    }



    // Just so we don't need to add on every job class
    public void onAdded() {
        ;
    }

    // Just so we don't need to add on every job class
    protected void onCancel() {
       ;
    }
}
