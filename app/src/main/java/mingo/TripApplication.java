package mingo;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Application;

import android.app.PendingIntent;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.util.Log;

import com.path.android.jobqueue.BaseJob;
import com.path.android.jobqueue.JobManager;
import com.path.android.jobqueue.di.DependencyInjector;
import com.path.android.jobqueue.log.CustomLogger;
import com.recording.models.DaoMaster;
import com.recording.models.DaoSession;
import com.path.android.jobqueue.config.Configuration;

import mingo.database.OpenHelper;
import mingo.jobs.JobWithContext;

public class TripApplication extends Application {

    private static TripApplication instance;

    private static DaoSession daoSession;
    private static Activity currentActivity;
    private static PendingIntent pendingIntent;

    public static AlarmManager getAlarmManager() {
        return alarmManager;
    }

    public static void setAlarmManager(AlarmManager alarmManager) {
        TripApplication.alarmManager = alarmManager;
    }

    public static PendingIntent getPendingIntent() {
        return pendingIntent;
    }

    public static void setPendingIntent(PendingIntent pendingIntent) {
        TripApplication.pendingIntent = pendingIntent;
    }

    private static AlarmManager alarmManager;

    public static int getCurrentTripCode() {
        return currentTripCode;
    }

    public static void setCurrentTripCode(int currentTripCode) {
        TripApplication.currentTripCode = currentTripCode;
    }

    private static int currentTripCode;
    private JobManager jobManager;
    private static Location location;
    public TripApplication() {
        instance = this;
    }

    public static TripApplication getInstance() {
        if (instance == null) {
            synchronized (TripApplication.class) {
                if (instance == null) {
                    instance = new TripApplication();
                }
            }
        }

        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        jobManager = new JobManager(this, getJobManagerConfiguration());
    }

    public void resetDatabase() {
        OpenHelper helper = OpenHelper.getInstance(this);
        SQLiteDatabase db = helper.getWritableDatabase();
        helper.resetDatabase(db);
    }

    public static Activity getCurrentActivity() {
        return currentActivity;
    }

    public static void setCurrentActivity(Activity currentActivity) {
        TripApplication.currentActivity = currentActivity;
    }



    public DaoSession getDaoSession() {
        if (daoSession == null) {
            synchronized (TripApplication.class) {
                if (daoSession == null) {
                    DaoMaster daoMaster = new DaoMaster(
                            OpenHelper.getInstance(getApplicationContext()).getWritableDatabase()
                    );
                    daoSession = daoMaster.newSession();
                }
            }
        }

        return daoSession;
    }

    // this is to kill old session and create a new one.
    public DaoSession getNewDaoSession() {
        daoSession = null;
        return getDaoSession();
    }

    public static Location getLocation() {
        return location;
    }

    public static void setLocation(Location location) {
        TripApplication.location = location;
    }
    private Configuration getJobManagerConfiguration() {
        return new Configuration.Builder(this)
                .customLogger(new CustomLogger() {
                    private static final String TAG = "JOBS";
                    @Override
                    public boolean isDebugEnabled() {
                        return true;
                    }

                    @Override
                    public void d(String text, Object... args) {
                        Log.d(TAG, String.format(text, args));
                    }

                    @Override
                    public void e(Throwable t, String text, Object... args) {
                        Log.e(TAG, String.format(text, args), t);
                    }

                    @Override
                    public void e(String text, Object... args) {
                        Log.e(TAG, String.format(text, args));
                    }
                })
                .minConsumerCount(1)//always keep at least one consumer alive
                .maxConsumerCount(3)//up to 3 consumers at a time
                .loadFactor(3)//3 jobs per consumer
                .consumerKeepAlive(120)//wait 2 minutes
                .injector(new DependencyInjector() {
                    @SuppressWarnings("deprecation")
                    @Override
                    public void inject(BaseJob job) {
                        if (job instanceof JobWithContext) {
                            ((JobWithContext) job).setContext(getApplicationContext());
                        }
                    }
                })
                .build();
    }

    public JobManager getJobManager() {
        return jobManager;
    }

}