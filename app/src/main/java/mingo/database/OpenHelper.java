package mingo.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.recording.models.DaoMaster;


public class OpenHelper extends SQLiteOpenHelper {

    private static OpenHelper instance;

    public static OpenHelper getInstance(Context context) {
        if (instance == null) {
            synchronized (OpenHelper.class) {
                if (instance == null) {
                    instance = new OpenHelper(context, "wally", null);
                }
            }
        }

        return instance;
    }

    public OpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory) {
        super(context, name, factory, DaoMaster.SCHEMA_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        DaoMaster.createAllTables(db, false);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            DaoMaster.dropAllTables(db, true);
            DaoMaster.createAllTables(db, false);
        }


    public void resetDatabase(SQLiteDatabase db) {
        DaoMaster.dropAllTables(db, true);
        DaoMaster.createAllTables(db, true);

    }
}
