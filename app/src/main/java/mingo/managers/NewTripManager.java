package mingo.managers;

import android.content.Context;

import com.recording.models.NewTrip;
import com.recording.models.NewTripDao;
import com.recording.models.Trip;
import com.recording.models.TripDao;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import de.greenrobot.dao.query.Query;
import mingo.TripApplication;


/**
 * Created by gaurav on 10/04/15.
 *
 */
public class NewTripManager {


    public static NewTrip load(Context ctx, long tripId) {
        return getNewTripDao(ctx).load(tripId);
    }

    public static List<NewTrip> loadAll(Context ctx) {
        return getNewTripDao(ctx).loadAll();
    }



    public static long count(Context ctx) {
        return getNewTripDao(ctx).count();
    }

//    public static Trip findByTripId(Context ctx, int tripId) {
//        Query<Trip> query = getTripDao(ctx).queryBuilder()
//                .where(TripDao.Properties.TripId.eq(tripId)).build();
//        return query.unique();
//    }


    public static void insertOrReplace(Context ctx, NewTrip trip) {
        getNewTripDao(ctx).insertOrReplace(trip);
    }





    public static void delete(Context ctx, NewTrip trip) {
        getNewTripDao(ctx).delete(trip);
    }


    public static NewTrip findByLocalId(Context ctx, Long id) {
        Query<NewTrip> query = getNewTripDao(ctx).queryBuilder()
                .where(NewTripDao.Properties.Id.eq(id)).build();
        return query.unique();
    }


    private static NewTripDao getNewTripDao(Context c) {
        return ((TripApplication) c.getApplicationContext()).getDaoSession().getNewTripDao();
    }

    public static NewTrip isAnyTripRunningAlready(Context ctx){
        Query<NewTrip> query = getNewTripDao(ctx).queryBuilder().where(NewTripDao.Properties.Tripon.eq(true)).build();
        return query.unique();
    }

    public static NewTrip isAnyTripScheduled(NewTrip currentTrip,Context context){
        List<NewTrip> futureTrips = findFutureTrips(context);
        NewTrip existingRunningTrip = null;
        Boolean flag = false;
        for(NewTrip trip : futureTrips){
            Date fromDate = trip.getStartingtime();
            Date endDate = trip.getEndtime();
            if(currentTrip.getStartingtime().compareTo(fromDate)>=0 && currentTrip.getStartingtime().compareTo(endDate)<=0){
                existingRunningTrip = trip;
                break;
            }
        }

        return existingRunningTrip;
    }

    public static List<NewTrip> findFutureTrips(Context ctx){
        Calendar newCalendar = Calendar.getInstance();
        Date date = newCalendar.getTime();
        Query<NewTrip> query = getNewTripDao(ctx).queryBuilder().where(NewTripDao.Properties.Startingtime.ge(date), NewTripDao.Properties.Endtime.ge(date)).build();
        return query.list();
    }


}
