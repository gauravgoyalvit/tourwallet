package mingo.managers;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.recording.models.Coordinates;
import com.recording.models.CoordinatesDao;
import com.recording.models.Pics;
import com.recording.models.PicsDao;
import com.recording.models.Trip;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import de.greenrobot.dao.query.Query;
import de.greenrobot.dao.query.QueryBuilder;
import mingo.TripApplication;
import mingo.jobs.JobPriority;


/**
 * Created by gaurav on 10/04/15.
 *
 */
public class PicsManager extends Manager {

private static String[] filterText;

    public static Pics load(Context ctx, long tripId) {
        return getPicsDao(ctx).load(tripId);
    }

    public static List<Pics> loadAll(Context ctx) {
        return getPicsDao(ctx).loadAll();
    }



    public static long count(Context ctx) {
        return getPicsDao(ctx).count();
    }

//    String sql = "SELECT SUM(amount) FROM income WHERE `date` BETWEEN ? AND ?";
//    String[] params = new String[] {
//            String.valueOf(start.getTime()),
//            String.valueOf(end.getTime())
//    };

//    Cursor c = getDb(ctx).rawQuery(sql, null);
//    int count = c.getCount();
//    months = new String[count];
//    if (count > 0) {
//        c.moveToFirst();
//        int i = 0;
//        while (!c.isAfterLast()) {
//            String monthStr = c.getString(1) + '-' + c.getString(2);
//            months[i++] = monthStr;
//            c.moveToNext();
//        }
//    }
//    c.close();
//    return months;
//
//    Cursor c = getDb(ctx).rawQuery(sql, params);
//    c.moveToFirst();
//    double sum = c.getDouble(0);
//    c.close();
//    return sum;

//    public static List<Pics> findByTripId(Context ctx, int tripId) {
//        String sql = "SELECT   FROM pics group by countryname";
//        Cursor c = getDb(ctx).rawQuery(sql,null);
//        ArrayList<Pics> picsArrayList = new ArrayList<>();
//        int count = c.getCount();
//        if(count>0)
//        {
//            c.moveToFirst();
//            int i=0;
//            while (!c.isAfterLast()){
//
//            }
//        }
//
//
//    }

    public static List<Pics> findByTripId(Context ctx, long tripId) {
        Query<Pics> query = getPicsDao(ctx).queryBuilder()
                .where(PicsDao.Properties.Pictripid.eq(tripId) ).build();
        return query.list();
    }

    public static void getHighestLevel(Context ctx,long id){

        String sql = "SELECT  COUNT(*),countryname FROM pics WHERE pictripid = "+ id+" GROUP BY countryname";
        int code = JobPriority.ALLPHOTO;
        Cursor c = getDb(ctx).rawQuery(sql,null);
        if(c.getCount()<=1) {
            String sql_admin = "SELECT  COUNT(*),adminarea FROM pics WHERE pictripid = "+ id+" GROUP BY adminarea";
            c = getDb(ctx).rawQuery(sql_admin, null);
            if(c.getCount()<=1){
            String sql_locality = "SELECT  COUNT(*),locality FROM pics WHERE pictripid = "+ id+" GROUP BY locality";
            c = getDb(ctx).rawQuery(sql_locality, null);
                if(c.getCount()<=1){
                    code =JobPriority.ALLPHOTO;
                }
                else
                    code = JobPriority.LOCALITYAREALEVELPHOTO;
            }
            else {
                code = JobPriority.ADMINAREALEVELPHOTO;
            }
        }
        else {
            code = JobPriority.COUNTRYLEVELPHOTO;
        }
        filterText = new String[c.getCount()];
        if (c.getCount() > 0) {
            c.moveToFirst();
            int i = 0;
            while (!c.isAfterLast()) {
                String filterStr = c.getString(1);
                filterText[i++] = filterStr;
                c.moveToNext();
            }
        }
        c.close();
        TripApplication.setCurrentTripCode(code);
    }


    public static String[] getCountryOption(Context ctx,long id){
        String country[]=null;
        String countries[]=null;

        String sql = "SELECT  COUNT(*),countryname FROM pics WHERE pictripid = "+ id+" GROUP BY countryname";
        Cursor c = getDb(ctx).rawQuery(sql,null);
        if (c.getCount() > 0) {
            country = new String[c.getCount()];
            c.moveToFirst();
            int i = 0;
            while (!c.isAfterLast()) {
                String filterStr = c.getString(1);
                country[i++] = filterStr;
                c.moveToNext();
            }
        }
        c.close();
        if(country[0]==null){
            countries = new String[c.getCount()];
            for(int i=1;i<country.length;i++){
                countries[i-1] = country[i];
            }
            countries[country.length-1] = null;
            return countries;
        }
        else {
            return country;
        }
    }

    public static int getCountryCount(Context ctx,String countryName,long id){
        String country[]=null;
        List<Pics> picses = findByTripId(ctx,id);
        String sql = "SELECT  * FROM pics WHERE countryname LIKE '%" +countryName + "%'" + " AND pictripid = "+ id ;
        //String sql = "SELECT  COUNT(*) FROM pics";
        Cursor c = getDb(ctx).rawQuery(sql,null);
        int cnt = c.getCount();
        c.close();
        return cnt;
    }

    public static String[] getCityOption(Context ctx,long id){
        String city[]=null;
        String cities[]=null;
        String sql = "SELECT  COUNT(*),adminarea FROM pics WHERE pictripid = "+ id+" GROUP BY adminarea";
        Cursor c = getDb(ctx).rawQuery(sql,null);
        if (c.getCount() > 0) {
            city = new String[c.getCount()];
            c.moveToFirst();
            int i = 0;
            while (!c.isAfterLast()) {
                String filterStr = c.getString(1);
                city[i++] = filterStr;
                c.moveToNext();
            }
        }
        if(city[0] == null){
            cities = new String[c.getCount()];
            for(int i=1;i<cities.length;i++){
                cities[i-1] = city[i];
            }
            cities[city.length-1] = null;
            return cities;
        }
        else {
            return city;
        }
    }

    public static int getCityCount(Context ctx,String cityName,long id){
        String country[]=null;
        String sql = "SELECT  COUNT(*) FROM pics WHERE adminarea LIKE '%" +cityName + "%'"+" AND pictripid = " + id ;

        Cursor c = getDb(ctx).rawQuery(sql,null);
        int cnt = c.getCount();
        c.close();
        return cnt;
    }

    public static String[] getLocalityOption(Context ctx,long id){
        List<Pics> picses = findByTripId(ctx,id);
        String locality[]=null;
        String localities[]=null;
        String sql = "SELECT COUNT(*),locality FROM pics WHERE pictripid = "+ id+" GROUP BY locality";
        Cursor c = getDb(ctx).rawQuery(sql,null);
        if (c.getCount() > 0) {
            locality = new String[c.getCount()];
            c.moveToFirst();
            int i = 0;
            while (!c.isAfterLast()) {
                String filterStr = c.getString(1);
                locality[i++] = filterStr;
                c.moveToNext();
            }
        }
        c.close();
        if(locality[0]==null){
            localities = new String[c.getCount()];
            for(int i=1;i<localities.length;i++){
                localities[i-1] = locality[i];
            }
            localities[locality.length-1] = null;
            return localities;
        }
        else {
            return locality;
        }
    }

    public static int getLocalityCount(Context ctx,String locality_,long id){
        String country[]=null;
        String sql = "SELECT  COUNT(*) FROM pics WHERE locality LIKE '%" +locality_ + "%'" +" AND pictripid = " + id;

        Cursor c = getDb(ctx).rawQuery(sql,null);
        int cnt = c.getCount();
        c.close();
        return cnt;
    }


    public static String[] returnResultAsPerFilter(Context ctx,String country,String city,String local,long id){
        String pics[]=null;
        String sql = "SELECT  picname FROM pics WHERE countryname = ? AND adminarea = ? AND locality = ?  AND pictripid = ?" ;
        String[] params = new String[] {
                country,
                city,local,String.valueOf(id)
        };
        Cursor c = getDb(ctx).rawQuery(sql,params);
        if (c.getCount() > 0) {
            pics = new String[c.getCount()];
            c.moveToFirst();
            int i = 0;
            while (!c.isAfterLast()) {
                String filterStr = c.getString(1);
                pics[i++] = filterStr;
                c.moveToNext();
            }
        }
        c.close();
        return pics;
    }


    public static void getPhotos(Context ctx){

    }

    public static HashMap<String,List<Pics>> returnPics(Context ctx,int tripId){
        QueryBuilder qb = getPicsDao(ctx).queryBuilder();

        HashMap<String,List<Pics>> list = new HashMap<>();
        // have to iterate through all photos and create a
        // put this as one more argument.
    //    PicsDao.Properties.PicTripId.eq(5);
        int code = TripApplication.getCurrentTripCode();
        int count = filterText.length;


        for(int i =0;i<count;i++) {
            if (code == JobPriority.ALLPHOTO || code == JobPriority.LOCALITYAREALEVELPHOTO) {
                qb.where(
                        PicsDao.Properties.Locality.like("%" + filterText[i] + "%"));
                qb.build();
            } else if (code == JobPriority.ADMINAREALEVELPHOTO) {
                qb.where(
                        PicsDao.Properties.Adminarea.like("%" + filterText[i] + "%"));
            } else {
                qb.where(
                        PicsDao.Properties.Countryname.like("%" + filterText[i] + "%"));
            }
            qb.build();


            List<Pics> list_ = qb.list();
            list.put(filterText[i],list_);
        }
        return list;
    }


    public static String[] returnPicsAsPerDateFilter(Context ctx, Date start, Date end,long id) {
        String sql = "SELECT picname FROM pics WHERE pictripid = ? AND `photoDate` BETWEEN ? AND ?";
        String[] pics=null;
        String[] params = new String[] {
                String.valueOf(id),
                String.valueOf(start.getTime()),
                String.valueOf(end.getTime())
        };

        Cursor c = getDb(ctx).rawQuery(sql, params);
        if (c.getCount() > 0) {
            pics = new String[c.getCount()];
            c.moveToFirst();
            int i = 0;
            while (!c.isAfterLast()) {
                String filterStr = c.getString(1);
                pics[i++] = filterStr;
                c.moveToNext();
            }
        }
        c.close();
        return pics;
    }

    public static void returnDateWisePics(List<Pics> picses){
        List<Pics> list = picses;

    }

    public static void insertOrReplace(Context ctx, Pics pics) {
        getPicsDao(ctx).insertOrReplace(pics);
    }





    public static void delete(Context ctx, Pics pics) {
        getPicsDao(ctx).delete(pics);
    }





    private static PicsDao getPicsDao(Context c) {
        return ((TripApplication) c.getApplicationContext()).getDaoSession().getPicsDao();
    }


}
