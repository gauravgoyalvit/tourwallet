package mingo.managers;

import android.content.Context;

import com.recording.models.Trip;
import com.recording.models.TripDao;

import java.util.List;

import de.greenrobot.dao.query.Query;
import mingo.TripApplication;


/**
 * Created by gaurav on 10/04/15.
 *
 */
public class TripManager {


    public static Trip load(Context ctx, long tripId) {
        return getTripDao(ctx).load(tripId);
    }

    public static List<Trip> loadAll(Context ctx) {
        return getTripDao(ctx).loadAll();
    }



    public static long count(Context ctx) {
        return getTripDao(ctx).count();
    }

//    public static Trip findByTripId(Context ctx, int tripId) {
//        Query<Trip> query = getTripDao(ctx).queryBuilder()
//                .where(TripDao.Properties.TripId.eq(tripId)).build();
//        return query.unique();
//    }


    public static void insertOrReplace(Context ctx, Trip trip) {
        getTripDao(ctx).insertOrReplace(trip);
    }





    public static void delete(Context ctx, Trip trip) {
        getTripDao(ctx).delete(trip);
    }





    private static TripDao getTripDao(Context c) {
        return ((TripApplication) c.getApplicationContext()).getDaoSession().getTripDao();
    }


}
