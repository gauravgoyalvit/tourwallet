package mingo.managers;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import mingo.tripwallet.R;
import mingo.tripwallet.TripAddNewPlacePopupActivity;

/**
 * Created by Ashish Prasad on 10/11/15.
 */
public class NotificationMessageManager {

    private NotificationMessageManager(){}

    private static  final  int showTripRecordNotificationId = 001;

    public static void showTripRecordNotification(Context context){
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setSmallIcon(R.drawable.ic_plusone_standard_off_client);
        builder.setContentTitle("Hey, mind to start the trip");
        builder.setContentText("Mind to give a trip name to organize your photos");

        Intent popUpIntent = new Intent(context, TripAddNewPlacePopupActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(TripAddNewPlacePopupActivity.class);

        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(popUpIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(resultPendingIntent);

        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(showTripRecordNotificationId, builder.build());
    }
}
