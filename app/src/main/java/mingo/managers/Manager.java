package mingo.managers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import mingo.database.OpenHelper;

/**
 * Created by wael on 2/4/15.
 */
public class Manager {
    protected static SQLiteDatabase getDb(Context ctx) {
        return OpenHelper.getInstance(ctx).getWritableDatabase();
    }
}
