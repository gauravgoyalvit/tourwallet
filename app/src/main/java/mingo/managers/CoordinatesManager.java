package mingo.managers;

import android.content.Context;

import com.google.android.gms.maps.model.LatLngBounds;
import com.recording.models.Coordinates;
import com.recording.models.CoordinatesDao;
import com.recording.models.NewTrip;

import java.util.List;

import de.greenrobot.dao.query.Query;
import mingo.TripApplication;


/**
 * Created by gaurav on 10/04/15.
 *
 */
public class CoordinatesManager {


    public static Coordinates load(Context ctx, long tripId) {
        return getCoordinatesDao(ctx).load(tripId);
    }

    public static List<Coordinates> loadAll(Context ctx) {
        return getCoordinatesDao(ctx).loadAll();
    }



    public static long count(Context ctx) {
        return getCoordinatesDao(ctx).count();
    }

    public static List<Coordinates> findByTripId(Context ctx, long tripId) {
        Query<Coordinates> query = getCoordinatesDao(ctx).queryBuilder()
                .where(CoordinatesDao.Properties.TripId.eq(tripId)).build();
        return query.list();
    }

    public static void insertOrReplace(Context ctx, Coordinates coordinates) {
        getCoordinatesDao(ctx).insertOrReplace(coordinates);
    }

    public static void delete(Context ctx, Coordinates coordinates) {
        getCoordinatesDao(ctx).delete(coordinates);
    }

    public static LatLngBounds getCoordinatesBounds(Context ctx,Long id) {
        LatLngBounds.Builder boundBuilder = new LatLngBounds.Builder();
        List<Coordinates> allCoordinates = CoordinatesManager.findByTripId(ctx, id);
        for (Coordinates coordinate:allCoordinates) {
            boundBuilder.include(coordinate.getLatLng());
        }

        if (allCoordinates.size() == 0){
            return null;
        }
        else{
            return boundBuilder.build();
        }
    }

    private static CoordinatesDao getCoordinatesDao(Context c) {
        return ((TripApplication) c.getApplicationContext()).getDaoSession().getCoordinatesDao();
    }


}
