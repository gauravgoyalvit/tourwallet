package mingo.fragments;

import android.app.AlarmManager;
import android.app.FragmentManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;

import java.util.ArrayList;

import mingo.TripApplication;
import mingo.services.AlarmReceiver;
import mingo.tripwallet.NewTripActivity;
import mingo.tripwallet.R;
import mingo.tripwallet.TripAddNewDialog;
import mingo.tripwallet.TripListRecyclerViewAdapter;
import mingo.tripwallet.TripListRecyclerViewAdapterModal;
import mingo.util.Constants;

/**
 * Created by Sunil on 09-Oct-15.
 */
public class HomeFragment extends Fragment implements TripAddNewDialog.TripAddNewDialogListener{

    private static HomeFragment mInstance;
    private View mRootView;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static final String DIALOG_ERROR = "dialog_error";
    private FloatingActionButton mFAB;
    private LocationRequest mLocationRequest;
    private ArrayList<TripListRecyclerViewAdapterModal> myDataset;
    private static final long INTERVAL = 1000 * 10;
    private static final long FASTEST_INTERVAL = 1000 * 5;

    // location related
    private static final int REQUEST_RESOLVE_ERROR = 1001;
    private GoogleApiClient mGoogleApiClient;
    private boolean mResolvingError = false;
    private static final String STATE_RESOLVING_ERROR = "state_resolving_error";
    Location mCurrentLocation;
    String mLastUpdateTime;
    private PendingIntent pendingIntent;

    private Location mLocation;


    public static HomeFragment getInstance(){
        if (mInstance == null){
            mInstance = new HomeFragment();
        }
        return mInstance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_home, container, false);

//        Intent alarmIntent = new Intent(getActivity(), AlarmReceiver.class);
//        pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, alarmIntent, 0);
     //   start();

        mResolvingError = savedInstanceState != null && savedInstanceState.getBoolean(STATE_RESOLVING_ERROR, false);

        myDataset = new ArrayList<TripListRecyclerViewAdapterModal>();
        for (int index =0; index < 5; index++){
            TripListRecyclerViewAdapterModal model = new TripListRecyclerViewAdapterModal("My Trip"+1, "http://static-dev-climbing.s3.amazonaws.com/wp-content/uploads/2013/07/Van-Road-Trip-Long-Exposure-475.jpg", "12 May 2015", "Goa India");
            myDataset.add(model);

        }

        mFAB = (FloatingActionButton)mRootView. findViewById(R.id.trip_list_activity_float_button);
        mFAB.setOnClickListener(fabClickListener());

        mRecyclerView = (RecyclerView)mRootView. findViewById(R.id.trip_list_activity_recyclerView);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)

        return mRootView;
    }

    //region FAB Click Listener
    private View.OnClickListener fabClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddNewDialog();
            }
        };
    }
    //endregion

    private void showAddNewDialog() {
       startActivity(new Intent(getActivity(), NewTripActivity.class));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void addedTrip(TripListRecyclerViewAdapterModal trip) {
        if (trip != null) {
            myDataset.add(trip);
            mAdapter.notifyDataSetChanged();
        }
    }


    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getContext());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();
            return false;
        }
    }

    public void start() {
        AlarmManager manager = (AlarmManager)getActivity(). getSystemService(getActivity().ALARM_SERVICE);
        int interval = Constants.AlarmTriggerInterval;

        manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
        Toast.makeText(getContext(), "Alarm Set", Toast.LENGTH_SHORT).show();
    }

    public void cancel() {
        AlarmManager manager = (AlarmManager)getActivity(). getSystemService(getActivity().ALARM_SERVICE);
        manager.cancel(pendingIntent);
        Toast.makeText(getContext(), "Alarm Canceled", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        TripApplication.setCurrentActivity(getActivity());
        mAdapter = new TripListRecyclerViewAdapter(getActivity(), myDataset);
        mRecyclerView.setAdapter(mAdapter);
    }
}
