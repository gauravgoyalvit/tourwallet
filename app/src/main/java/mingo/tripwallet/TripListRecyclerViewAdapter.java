package mingo.tripwallet;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.recording.models.NewTrip;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import mingo.managers.NewTripManager;
import mingo.util.Logger;
import mingo.util.Utilities;

/**
 * Created by Ashish Prasad on 10/2/15.
 */
public class TripListRecyclerViewAdapter extends RecyclerView.Adapter<TripListRecyclerViewAdapter.ViewHolder> {

    private List<NewTrip> mDataset;
    private Context mContext;
    private String photoName;

    //region TripList - Card View Holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTripName;
        private TextView mTripDate;
        private TextView mtripLocation;
        private ImageView mTrimImageView;
        private ImageView endTrip;
        private ImageView mapButton;
        private ImageView photosButton;


        public ViewHolder(View v) {
            super(v);
            mTripName = (TextView) v.findViewById(R.id.tripName);
            mTripDate = (TextView) v.findViewById(R.id.tripdate);
            photosButton = (ImageView) v.findViewById(R.id.photosicon);
            mtripLocation = (TextView) v.findViewById(R.id.tripLocation);
            mTrimImageView = (ImageView) v.findViewById(R.id.tripImage);
            endTrip = (ImageView) v.findViewById(R.id.deleteicon);
            mapButton = (ImageView) v.findViewById(R.id.mapicon);
        }
    }
    //endregion

    public TripListRecyclerViewAdapter(Context context, ArrayList<TripListRecyclerViewAdapterModal> myDataset) {
        this.mDataset = NewTripManager.loadAll(context);
        this.mContext = context;
        this.photoName = "http://static-dev-climbing.s3.amazonaws.com/wp-content/uploads/2013/07/Van-Road-Trip-Long-Exposure-475.jpg";
    }

    //region TripListRecyclerViewAdapter
    // Create new views (invoked by the layout manager)
    @Override
    public TripListRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                     int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.trip_list_item_card_view, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (position >= mDataset.size()) {
            Logger.e(this.getClass().getName(), "Position index out of bound on BindViewHolder");
            return;
        }
        final NewTrip data = mDataset.get(position);
        holder.endTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utilities.endTrip(data,mContext);
                Toast.makeText(mContext,"ended Trip",Toast.LENGTH_SHORT);
            }
        });
        holder.mapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(mContext,TripDetailActivity.class);
                myIntent.putExtra("localId", data.getId()); //Optional parameters
                mContext.startActivity(myIntent);
            }
        });
        holder.photosButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(mContext,PhotoActivity.class);
                myIntent.putExtra("localId", data.getId()); //Optional parameters
                mContext.startActivity(myIntent);
            }
        });
        holder.mTripName.setText(data.getName());
        holder.mTripDate.setText(data.getStartingtime().toString());
        holder.mtripLocation.setText(data.getLocations());
        if (data.getPhotoname() != null && !data.getPhotoname().isEmpty()){

            Picasso.with(mContext)
                    .load(data.getPhotoname())
                    .centerCrop()
                    .resize(400, 400)
                    .placeholder(R.drawable.trip_holder)
                    .error(R.drawable.trip_holder)
                    .into( holder.mTrimImageView);
        }
        else {
            Picasso.with(mContext)
                    .load(photoName)
                    .centerCrop()
                    .resize(400, 400)
                    .placeholder(R.drawable.trip_holder)
                    .error(R.drawable.trip_holder)
                    .into( holder.mTrimImageView);
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return (mDataset == null ? 0 : mDataset.size());
    }
    //endregion
}
