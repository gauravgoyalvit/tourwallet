package mingo.tripwallet;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import fr.tvbarthel.lib.blurdialogfragment.BlurDialogFragment;

/**
 * Created by Ashish Prasad on 10/2/15.
 */
public class TripAddNewDialog extends BlurDialogFragment implements View.OnClickListener {

    public interface TripAddNewDialogListener {
        public void addedTrip(TripListRecyclerViewAdapterModal trip);
    }

    private TextView mTripNameTextView;
    private EditText mTripNameEditText;
    private Button mTripAddConfirmButton;

    public TripAddNewDialog() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.trip_add_new_dialog_fragment, container);
        mTripNameTextView = (TextView) view.findViewById(R.id.trip_add_new_dialog_trip_name_text_view);
        mTripNameEditText = (EditText) view.findViewById(R.id.trip_add_new_dialog_trip_name_edit_text);
        mTripAddConfirmButton = (Button) view.findViewById(R.id.trip_add_new_confirm_button);

        mTripAddConfirmButton.setOnClickListener(this);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        view.setBackgroundDrawable(new ColorDrawable(0));
        getDialog().setTitle("New Journey");

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.trip_add_new_confirm_button: {
                TripListRecyclerViewAdapterModal tripModal = new TripListRecyclerViewAdapterModal();
                tripModal.setTripName(mTripNameEditText.getText().toString());

                TripAddNewDialogListener listener = (TripAddNewDialogListener) getActivity();
                listener.addedTrip(tripModal);
                dismiss();
            }
            break;
        }
    }
}
