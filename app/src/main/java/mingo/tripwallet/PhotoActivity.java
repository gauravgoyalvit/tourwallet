package mingo.tripwallet;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.recording.models.Pics;

import java.util.ArrayList;
import java.util.List;

import mingo.TripApplication;
import mingo.jobs.JobPriority;
import mingo.managers.PicsManager;

/**
 * Created by kuliza190 on 23/9/15.
 */
public class PhotoActivity extends AppCompatActivity {

    private RecyclerView photoGrid;
    private int REQUEST_IMAGE_CAPTURE = 1;
    private Long id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.add_photo_activity);
        Intent intent = getIntent();
        id = intent.getLongExtra("localId",0);

        photoGrid = (RecyclerView) findViewById(R.id.photo_grid);
        List<Pics> list = PicsManager.loadAll(PhotoActivity.this);
        photoGrid.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this,3);
        photoGrid.setLayoutManager(gridLayoutManager);
        AddPhotoGridAdapter nearbyGridAdapter = new AddPhotoGridAdapter(getAllShownImagesPath(this,id), this);
        addSectionHeader(nearbyGridAdapter,id);
    }

    public void addSectionHeader(AddPhotoGridAdapter adapter ,long id){
        Context context = PhotoActivity.this;
        List<String> listOfImages = getAllShownImagesPath(PhotoActivity.this,id);
        PicsManager.getHighestLevel(context,id);
        int a = TripApplication.getCurrentTripCode();

        List<SectionedGridRecyclerViewAdapter.Section> sections =
                new ArrayList<SectionedGridRecyclerViewAdapter.Section>();

        String[] places = new String[100];
        if(a== JobPriority.COUNTRYLEVELPHOTO){

            places = PicsManager.getCountryOption(context,id);


            //Sections
            sections.add(new SectionedGridRecyclerViewAdapter.Section(0, places[0]));
            int count = 0;
            for(int i = 1;i<places.length;i++) {
                 count = count+PicsManager.getCountryCount(PhotoActivity.this,places[i-1],id);
                if(places[i]==null)
                    places[i]= "Others";
                sections.add(new SectionedGridRecyclerViewAdapter.Section(count, places[i]));
            }

        }
        else if(a== JobPriority.ADMINAREALEVELPHOTO){
            places = PicsManager.getCityOption(context,id);
            //Sections
            sections.add(new SectionedGridRecyclerViewAdapter.Section(0, places[0]));
            int count = 0;
            for(int i = 1;i<places.length;i++) {
                count = count+PicsManager.getCityCount(PhotoActivity.this, places[i - 1],id);
                if(places[i]==null)
                    places[i]= "Others";
                sections.add(new SectionedGridRecyclerViewAdapter.Section(count, places[i]));
            }

        }

        else if(a== JobPriority.LOCALITYAREALEVELPHOTO){
            places = PicsManager.getLocalityOption(context,id);
            //Sections
            sections.add(new SectionedGridRecyclerViewAdapter.Section(0, places[0]));
            int count = 0;
            for(int i = 1;i<places.length;i++) {
                count = count+PicsManager.getLocalityCount(PhotoActivity.this, places[i - 1],id);
                if(places[i]==null)
                    places[i]= "Others";
                sections.add(new SectionedGridRecyclerViewAdapter.Section(count, places[i]));
            }

        }

        else {
            places = PicsManager.getLocalityOption(context,id);
            //Sections
            sections.add(new SectionedGridRecyclerViewAdapter.Section(0, "ALL Photos"));
        }

        //This is the code to provide a sectioned list

        // sections.add(new SimpleSectionedGridRecyclerViewAdapter.Section(5,"Section 2"));

        //Add your adapter to the sectionAdapter
        SectionedGridRecyclerViewAdapter.Section[] dummy = new SectionedGridRecyclerViewAdapter.Section[sections.size()];
        SectionedGridRecyclerViewAdapter mSectionedAdapter = new
                SectionedGridRecyclerViewAdapter(PhotoActivity.this, R.layout.section_person, R.id.section_text,photoGrid, adapter);
        mSectionedAdapter.setSections(sections.toArray(dummy));

        //Apply this adapter to the RecyclerView
        photoGrid.setAdapter(mSectionedAdapter);
    }

    public  ArrayList<String> getAllShownImagesPath(Activity activity,long id) {
//        Uri uri;
//        Cursor cursor;
//        int column_index_data, column_index_folder_name;
        ArrayList<String> listOfAllImages = new ArrayList<String>();
//        String absolutePathOfImage = null;
//        uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
//
//        String[] projection = { MediaStore.MediaColumns.DATA,
//                MediaStore.Images.Media.BUCKET_DISPLAY_NAME };
//
//        cursor = activity.getContentResolver().query(uri, projection, null,
//                null, null);
//
//        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
//        column_index_folder_name = cursor
//                .getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
//        while (cursor.moveToNext()) {
//            absolutePathOfImage = cursor.getString(column_index_data);
//
//            listOfAllImages.add(absolutePathOfImage);
//        }

        List<Pics> list = PicsManager.findByTripId(PhotoActivity.this,id);
        for(int i=0;i<list.size();i++){
            listOfAllImages.add(list.get(i).getPicname());
        }
        return listOfAllImages;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_IMAGE_CAPTURE && data != null) {
                Toast.makeText(this, "Image Captured", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
