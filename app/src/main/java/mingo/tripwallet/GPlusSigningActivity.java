package mingo.tripwallet;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import mingo.util.NetworkUtil;
import mingo.util.SharedPreferenceUtils;
import mingo.util.Utilities;

/**
 * Created by sunil on 07-Oct-15.
 */
public class GPlusSigningActivity extends Activity implements View.OnClickListener,  GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{


    private String mUserEmailId;
    private SignInButton mSignButton;

    private static final int RC_SIGN_IN = 0;
    // Google client to communicate with Google
    private GoogleApiClient mGoogleApiClient;

    private boolean mIntentInProgress;
    private boolean signedInUser;
    private ConnectionResult mConnectionResult;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API, Plus.PlusOptions.builder().build())
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();


        setContentView(R.layout.activity_google_signin);

        mSignButton = (SignInButton) findViewById(R.id.sign_in_button);
        mSignButton.setOnClickListener(this);

    }

    protected void onStart() {
        super.onStart();

        mGoogleApiClient.connect();


    }

    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private void resolveSignInError() {
        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (!result.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this, 0).show();
            return;
        }

        if (!mIntentInProgress) {
            // store mConnectionResult
            mConnectionResult = result;

            if (signedInUser) {
                resolveSignInError();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        switch (requestCode) {
            case RC_SIGN_IN:
                if (responseCode == RESULT_OK) {
                    signedInUser = false;

                }
                mIntentInProgress = false;
                if (!mGoogleApiClient.isConnecting()) {
                    mGoogleApiClient.connect();
                }
                break;
        }
    }

    @Override
    public void onConnected(Bundle arg0) {
        signedInUser = false;
        getProfileInformation();

    }



    private void getProfileInformation() {

        try {
            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
                String personName = currentPerson.getDisplayName();
                String personPhotoUrl = currentPerson.getImage().getUrl();

                mUserEmailId = Plus.AccountApi.getAccountName(mGoogleApiClient);

                SharedPreferenceUtils.getInstance(GPlusSigningActivity.this).setUserEmail(mUserEmailId);
                SharedPreferenceUtils.getInstance(GPlusSigningActivity.this).setUserName(personName);
                SharedPreferenceUtils.getInstance(GPlusSigningActivity.this).setUserImage(personPhotoUrl);

                googlePlusLogout();

                startActivity(new Intent(GPlusSigningActivity.this, HomeScreenActivity.class));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int cause) {
        mGoogleApiClient.connect();

    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.sign_in_button) {
            //client.startSignIn();

            if (NetworkUtil.isNetworkAvailable(GPlusSigningActivity.this)){


                if (mGoogleApiClient.isConnected()) {

                  //  Toast.makeText(GPlusSigningActivity.this, "Already connected", Toast.LENGTH_LONG).show();
                    getProfileInformation();

                }
                else{
                    googlePlusLogin();
                }
            }
            else{
                Utilities.showAlert(GPlusSigningActivity.this, "Internet Error");
            }


        }
    }

    private void googlePlusLogin() {
        if (!mGoogleApiClient.isConnecting()) {
            mGoogleApiClient.connect();
            signedInUser = true;
            resolveSignInError();
        }
    }

    private void googlePlusLogout() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
        }

    }
}
