package mingo.tripwallet;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by kuliza190 on 23/9/15.
 */
public class AddPhotoGridAdapter extends RecyclerView.Adapter<AddPhotoGridAdapter.Holder> {

    private ArrayList<String> pathList;
    private Activity activity;
    private ImageView lastSelected;
    private ImageView lastMark;
    ImageView photo;
    private int REQUEST_IMAGE_CAPTURE = 1;

    public AddPhotoGridAdapter(ArrayList<String> pathList, Activity activity){
        this.pathList = pathList;
        this.activity = activity;
    }

    @Override
    public AddPhotoGridAdapter.Holder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.add_photo_grid_item, viewGroup, false);
        Holder holder = new Holder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(final AddPhotoGridAdapter.Holder holder, int i) {

//
//            File imgFile = new File(pathList.get(i));
//
//            if (imgFile.exists()) {
//
//                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
//
//                holder.photo.setImageBitmap(myBitmap);
//
//
//            }

        glideLoad(pathList.get(i));
        final int j = i;
        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.parse("file://" + pathList.get(j)), "image/*");
                activity.startActivity(intent);
            }
        });



    }



    @Override
    public int getItemCount() {
        return pathList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        ImageView checkMark;

        public Holder(View itemView) {
            super(itemView);
            photo = (ImageView) itemView.findViewById(R.id.photo);
        }
    }

    private void glideLoad(String imageUrl){
        String fileurl="file://" + imageUrl;
        Log.v("", "Image Url is: " + fileurl);
        if (fileurl!=null && !fileurl.isEmpty()) {
            Picasso.with(activity)
                    .load(fileurl)
                    .centerCrop()
                    .resize(200,200)
                    .placeholder(R.drawable.common_signin_btn_icon_focus_light)
                    .error(R.drawable.common_signin_btn_icon_focus_light)
                    .into(photo);
        }
    }
}
