package mingo.tripwallet;

/**
 * Created by Ashish Prasad on 10/2/15.
 */
public class TripListRecyclerViewAdapterModal {
    private String tripName;
    private String tripImage;
    private String tripDate;
    private String tripLocation;

    public TripListRecyclerViewAdapterModal() {
    }

    public TripListRecyclerViewAdapterModal(String tripName, String tripImage, String tripDate, String tripLocation) {
        this.tripName = tripName;
        this.tripImage = tripImage;
        this.tripDate = tripDate;
        this.tripLocation = tripLocation;

    }

    //region tripName Getter Setter
    public String getTripName() {
        return tripName;
    }

    public void setTripName(String tripName) {
        this.tripName = tripName;
    }
    //endregion


    public String getTripImage() {
        return tripImage;
    }

    public void setTripImage(String tripImage) {
        this.tripImage = tripImage;
    }

    public String getTripLocation() {
        return tripLocation;
    }

    public void setTripLocation(String tripLocation) {
        this.tripLocation = tripLocation;
    }

    public String getTripDate() {
        return tripDate;
    }

    public void setTripDate(String tripDate) {
        this.tripDate = tripDate;
    }
}
