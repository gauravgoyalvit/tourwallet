package mingo.tripwallet;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

/**
 * Implementation of App Widget functionality.
 */
public class TripAddPlaceWidget extends AppWidgetProvider {
    private static final String ACTION_UPDATE_CLICK =
            "mingo.tripwallet.addplacewidget.NEW_PLACE";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        final int N = appWidgetIds.length;
        for (int i = 0; i < N; i++) {
            updateAppWidget(context, appWidgetManager, appWidgetIds[i]);
        }
    }


    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                         int appWidgetId) {

        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.trip_add_place_widget);
        views.setOnClickPendingIntent(R.id.trip_add_place_widget_button, getPendingSelfIntent(context,
                ACTION_UPDATE_CLICK));

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    private PendingIntent getPendingSelfIntent(Context context, String action) {
        // An explicit intent directed at the current class (the "self").
        Intent intent = new Intent(context, getClass());
        intent.setAction(action);
        return PendingIntent.getBroadcast(context, 0, intent, 0);
    }


    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);

        if (ACTION_UPDATE_CLICK.equals(intent.getAction())) {
            onUpdate(context);
        }
    }

    private void onUpdate(Context context) {
//        FragmentManager fm = context.getActivity().getFragmentManager();
//        TripAddNewDialog tripAddNewDialog = new TripAddNewDialog();
//
//        Bundle args = new Bundle();
//        args.putString("tripName", "tripName");
//        tripAddNewDialog.setArguments(args);
//
//        tripAddNewDialog.show(fm, "tag");

        Intent popUpIntent = new Intent(context, TripAddNewPlacePopupActivity.class);
        popUpIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(popUpIntent);



//        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance
//                (context);
//
//        // Uses getClass().getName() rather than MyWidget.class.getName() for
//        // portability into any App Widget Provider Class
//        ComponentName thisAppWidgetComponentName =
//                new ComponentName(context.getPackageName(), getClass().getName()
//                );
//        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(
//                thisAppWidgetComponentName);
//        onUpdate(context, appWidgetManager, appWidgetIds);
    }

}


