package mingo.tripwallet;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class TripAddNewPlacePopupActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trip_add_new_place_popup_activity);

        String dialogText = "Dialog Box : " + (new Random().nextInt(100));
        TextView txt = (TextView) findViewById(R.id.trip_add_new_place_popup_place_name);
        txt.setText(dialogText);

        Button dismissbutton = (Button) findViewById(R.id.trip_add_new_place_popup_place_ok_button);
        dismissbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TripAddNewPlacePopupActivity.this.finish();
            }
        });
    }
}
