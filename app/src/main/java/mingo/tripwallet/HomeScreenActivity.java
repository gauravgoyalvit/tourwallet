package mingo.tripwallet;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import mingo.TripApplication;
import mingo.fragments.HomeFragment;
import mingo.services.AlarmReceiver;
import mingo.util.Constants;
import mingo.util.SharedPreferenceUtils;

/**
 * Created by Sunil on 08-Oct-15.
 */
public class HomeScreenActivity extends AppCompatActivity{

    private static final String TAG= "HomeScreenActivity";
    private Context mContext;
    private DrawerLayout mDrawerLayout;

    private TextView mTextviewUserName;
    private TextView mTextViewEmail;
    private ImageView mProfileImageView;
    private PendingIntent pendingIntent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homescreen);
        String [] PermissionsLocation =
                {
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.INTERNET,
                        Manifest.permission.RECEIVE_BOOT_COMPLETED,
                        Manifest.permission.GET_ACCOUNTS,
                        Manifest.permission.CAMERA,
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.READ_EXTERNAL_STORAGE

                };
        requestPermissions(PermissionsLocation, 0);
        TripApplication.setCurrentActivity(HomeScreenActivity.this);
        Intent alarmIntent = new Intent(HomeScreenActivity.this, AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(HomeScreenActivity.this, 0, alarmIntent, 0);
        //start();
        mContext=this;

        final Toolbar toolbar= (Toolbar) findViewById(R.id.toolbar);

        mTextviewUserName = (TextView) findViewById(R.id.username);
        mTextViewEmail = (TextView) findViewById(R.id.useremail);
        mProfileImageView = (ImageView) findViewById(R.id.profileimage);

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu);

        String userEmail = SharedPreferenceUtils.getInstance(HomeScreenActivity.this).getUserEmail();
        String userName = SharedPreferenceUtils.getInstance(HomeScreenActivity.this).getUserName();
        String profileUrl = SharedPreferenceUtils.getInstance(HomeScreenActivity.this).getUserImage();

        mTextviewUserName.setText(userName);
        mTextViewEmail.setText(userEmail);
        if (profileUrl != null && !profileUrl.isEmpty()){

            Picasso.with(HomeScreenActivity.this)
                    .load(profileUrl)
                    .placeholder(R.drawable.logo)
                    .error(R.drawable.logo)
                    .into(mProfileImageView);
        }


        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }

        if (savedInstanceState == null){

            loadAddNoteFragment();
        }

    }

    // add note full screen
    private void loadAddNoteFragment(){
        HomeFragment noteFragment = HomeFragment.getInstance();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, noteFragment)
                .commit();
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the planet to show based on
        // position
        switch (menuItem.getItemId()) {
            case R.id.logout:

                SharedPreferenceUtils.getInstance(this).setToekn("");
                Intent intent = new Intent(HomeScreenActivity.this, GPlusSigningActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();

                break;

            case R.id.settings:

                // call settings screen

                break;

            case R.id.feed_back:

                StringBuffer buffer = new StringBuffer();
                buffer.append("mailto:");
                buffer.append("sunil.gupta676@gmail.com");
                buffer.append("?subject=");
                buffer.append(getResources().getString(R.string.app_name));
                buffer.append("&body=");

                String uriString = buffer.toString().replace(" ", "%20");

                startActivity(Intent.createChooser(new Intent(Intent.ACTION_SENDTO, Uri.parse(uriString)), "Feedback"));

                break;

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id==android.R.id.home){

                mDrawerLayout.openDrawer(GravityCompat.START);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void start() {
        AlarmManager manager = (AlarmManager) getSystemService(ALARM_SERVICE);
        int interval = Constants.AlarmTriggerInterval;

        manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
        Toast.makeText(HomeScreenActivity.this, "Alarm Set", Toast.LENGTH_SHORT).show();
    }

}
