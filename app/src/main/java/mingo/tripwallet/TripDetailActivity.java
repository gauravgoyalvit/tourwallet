package mingo.tripwallet;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.recording.models.Coordinates;
import com.recording.models.NewTrip;

import java.util.List;

import mingo.managers.CoordinatesManager;
import mingo.managers.NewTripManager;

public class TripDetailActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Long id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        id = intent.getLongExtra("localId",0);
        setContentView(R.layout.trip_detail_activity);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLngBounds bounds = CoordinatesManager.getCoordinatesBounds(getApplicationContext(),id);

        if (bounds != null) {
            LatLng center = bounds.getCenter();
            mMap.addMarker(new MarkerOptions().position(center).title("Center"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(center));
            mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 10));

            PolylineOptions polyLines = getPolyLineFromCoordinates(CoordinatesManager.loadAll(getApplicationContext()),5.0f, Color.RED);
            Polyline line = mMap.addPolyline(polyLines);
        }
    }

    private PolylineOptions getPolyLineFromCoordinates(List<Coordinates> coordinates, Float width, int color){
        PolylineOptions polylineOptions = new PolylineOptions();
        polylineOptions.width(width);
        polylineOptions.color(color);

        for (Coordinates coordinate:coordinates) {
            //TripDetailMapPointsModal tripMapModel = new TripDetailMapPointsModal(coordinate);
            polylineOptions.add(coordinate.getLatLng());
        }
        return polylineOptions;
    }
}
