package mingo.tripwallet;

import com.google.android.gms.maps.model.LatLng;
import com.recording.models.Coordinates;

/**
 * Created by Ashish Prasad on 10/10/15.
 */
public class TripDetailMapPointsModal {

    private LatLng latLng;
    public TripDetailMapPointsModal(Coordinates coordinate){
        this.latLng = coordinate.getLatLng();
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }
}
