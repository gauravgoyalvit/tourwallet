package mingo.tripwallet;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.security.Permission;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import android.app.DatePickerDialog.OnDateSetListener;

import com.recording.models.NewTrip;

import mingo.TripApplication;
import mingo.managers.NewTripManager;
import mingo.services.AlarmReceiver;
import mingo.services.TripValidReceiver;
import mingo.util.Constants;
import mingo.util.Utilities;

public class NewTripActivity extends AppCompatActivity {
    private DatePickerDialog fromDatePickerDialog;
    private TimePickerDialog fromTimePickerDialog;
    private SimpleDateFormat dateFormatter;
    private SimpleDateFormat timeFormatter;
    private EditText fromDateEtxt;
    private EditText fromTimeText;
    private EditText toDateEtxt;
    private EditText toTimeText;
    private EditText tripName;
    private EditText location;
    private CheckBox checkBox;
    private Boolean flag = false;
    int RequestLocationId = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_trip);
        TripApplication.setCurrentActivity(NewTripActivity.this);
        Toolbar mToolBar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolBar != null) {
            setSupportActionBar(mToolBar);
        }
        ImageView cancel_action = (ImageView) findViewById(R.id.cancel_action);
        cancel_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Calendar newCalendar = Calendar.getInstance();
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        timeFormatter = new SimpleDateFormat("K:mm a");
        fromDateEtxt = (EditText) findViewById(R.id.datepicker);
        fromTimeText = (EditText) findViewById(R.id.timePicker);
        checkBox = (CheckBox) findViewById(R.id.checkbox);
        final LinearLayout linearLayout = (LinearLayout) findViewById(R.id.fromDateAndTime);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    flag = false;
                    linearLayout.setAlpha(0.4f);
                } else {
                    flag = true;
                    linearLayout.setAlpha(1.0f);
                }
            }
        });
        tripName = (EditText) findViewById(R.id.tripNameId);
        location = (EditText) findViewById(R.id.tripLocation);
        fromDateEtxt.setText(dateFormatter.format(newCalendar.getTime()));
        fromTimeText.setText(timeFormatter.format(newCalendar.getTime()));

        toDateEtxt = (EditText) findViewById(R.id.toDatepicker);
        toTimeText = (EditText) findViewById(R.id.toTimePicker);


        fromDateEtxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag)
                    fromDatePicker(v).show();
            }
        });

        fromTimeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag)
                    fromTimePicker(v).show();
            }
        });

        toDateEtxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromDatePicker(v).show();
            }
        });

        toTimeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromTimePicker(v).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_trip, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.done:
                Date fromdate = convertStringToDate(fromDateEtxt, fromTimeText);
                Date todate = convertStringToDate(toDateEtxt, toTimeText);

                NewTrip newTrip_ = NewTripManager.isAnyTripRunningAlready(NewTripActivity.this);
                NewTrip trip = new NewTrip();
                trip.setName(tripName.getText().toString());
                trip.setLocations(location.getText().toString());
                trip.setStartingtime(fromdate);
                trip.setTripon(false);
                if (todate != null) {
                    trip.setEndtime(todate);
                }

                if (checkBox.isChecked() && newTrip_ != null) {
                    // for right now trips checking if any trip is running currently.
                    Utilities.showAlreadyExistingPopup(NewTripActivity.this, trip);
                } else {
                    // checking if any other trip has been scheuled for this time
                    NewTrip scheduledTrip = NewTripManager.isAnyTripScheduled(newTrip_, NewTripActivity.this);
                    if (scheduledTrip == null) {

                        // no trip has been scheduled for this time.
                        NewTripManager.insertOrReplace(NewTripActivity.this, trip);
                        Calendar cur_cal = new GregorianCalendar();
                        cur_cal.setTime(fromdate);

                        Intent alarmIntent = new Intent(NewTripActivity.this, TripValidReceiver.class);
                        alarmIntent.putExtra("localId", trip.getId());
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(NewTripActivity.this, 0, alarmIntent, 0);
                        AlarmManager manager = (AlarmManager) getSystemService(ALARM_SERVICE);
                        manager.set(AlarmManager.RTC_WAKEUP, cur_cal.getTimeInMillis(), pendingIntent);

                        Utilities.enableDeviceRecieverReceiver(NewTripActivity.this);
                        Utilities.enablePhotoRecieverReceiver(NewTripActivity.this);

                        // dummy pics for db
                        //Constants.setPics(NewTripActivity.this);

                        finish();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(NewTripActivity.this);
                        alert.setTitle("Existing Trip");
                        alert.setMessage("You have already scheduled" + scheduledTrip.getName() + "trip at this time,Please reschduled either of these");
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    }
                }
                return true;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public DatePickerDialog fromDatePicker(final View v) {
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(NewTripActivity.this, new OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                ((EditText) v).setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        return fromDatePickerDialog;
    }

    public TimePickerDialog fromTimePicker(final View v) {
        Calendar newCalendar = Calendar.getInstance();
        fromTimePickerDialog = new TimePickerDialog(NewTripActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String AM_PM;
                if (selectedHour < 12) {
                    AM_PM = "AM";
                } else {
                    AM_PM = "PM";
                }
                if (selectedMinute < 10)
                    ((EditText) v).setText(selectedHour + ":0" + selectedMinute + " " + AM_PM);
                else
                    ((EditText) v).setText(selectedHour + ":" + selectedMinute + " " + AM_PM);
            }
        }, newCalendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.MINUTE), true);//Yes 24 hour time
        fromTimePickerDialog.setTitle("Select Time");
        fromTimePickerDialog.show();
        return fromTimePickerDialog;
    }

    public static Date convertStringToDate(EditText dateText, EditText timeText) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm aa");
        if (dateText.getText().toString().equals("")) {
            return null;
        }
        String date1 = "03/26/2012 11:49:00 AM";
        String time = timeText.getText().toString();
        if (time.equals("")) {
            time = "11:59 PM";
        }
        String dateString = dateText.getText().toString() + " " + time;
        Date fromdate = null;
        try {
            fromdate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            ;
        }
        return fromdate;
    }

    public static void checkIfTripExistsAtSameTime() {

    }
}
