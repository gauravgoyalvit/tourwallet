package mingo.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.recording.models.NewTrip;

import mingo.managers.NewTripManager;
import mingo.util.Constants;
import mingo.util.Logger;


/**
 * @author gaurav
 *         <p/>
 *         Broadcast reciever, starts when the device gets starts.
 *         Start your repeating alarm here.
 */
public class DeviceBootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            /* Setting the alarm here */

            // on rebooting of device checking if a trip was in running state. If yes, starting the alarm triggering.
            NewTrip newTrip_ = NewTripManager.isAnyTripRunningAlready(context);
            if(newTrip_!=null) {
                Intent alarmIntent = new Intent(context, AlarmReceiver.class);

                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);

                AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

                int interval = Constants.AlarmTriggerInterval;

                manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
                Logger.d("alarm set again", "dw");

                Toast.makeText(context, "Alarm Set", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
