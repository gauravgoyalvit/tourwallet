package mingo.services;

import com.google.android.gms.common.api.PendingResult;

import android.app.Service;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.text.format.DateUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.common.api.Status;

import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;

import com.google.android.gms.location.LocationListener;
import com.recording.models.Coordinates;
import com.recording.models.NewTrip;
import com.recording.models.Trip;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import mingo.TripApplication;
import mingo.managers.CoordinatesManager;
import mingo.managers.NewTripManager;
import mingo.tripwallet.TripListRecyclerViewAdapterModal;
import mingo.util.Constants;
import mingo.util.Utilities;

/**
 * Created by gaurav on 03/10/15.
 */

// service starts when user click on start trip button.
// service keeps running and insert data into db.
public class LocationService extends Service implements LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private LocationRequest mLocationRequest;
    private ArrayList<TripListRecyclerViewAdapterModal> myDataset;
    private static final long INTERVAL = Constants.locationServiceInterval;
    private static final long FASTEST_INTERVAL = Constants.locationServiceFastInterval;

    // location related

    private GoogleApiClient mGoogleApiClient;
    Location mCurrentLocation;
    String mLastUpdateTime;


    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());


        // mapping of location
        Coordinates coordinates = new Coordinates();
        NewTrip trip = NewTripManager.isAnyTripRunningAlready(getApplicationContext());
        coordinates.setNewTrip(trip);
        coordinates.setTripId(trip.getId());
        coordinates.setAccuracy(mCurrentLocation.getAccuracy());
        coordinates.setAltitude(mCurrentLocation.getAltitude());
        coordinates.setBearing(mCurrentLocation.getBearing());
        coordinates.setLatitude(mCurrentLocation.getLatitude());
        coordinates.setLongitude(mCurrentLocation.getLongitude());
        coordinates.setProvider(mCurrentLocation.getProvider());
        coordinates.setRealtimeElipsedNanos(mCurrentLocation.getElapsedRealtimeNanos());
        coordinates.setSpeed(mCurrentLocation.getSpeed());
        coordinates.setTime(mCurrentLocation.getTime());


        // inserting location data into db;

        // check if new longitude and latitude falls out of 750 meters range
        if (Utilities.isLocationValid(getApplicationContext(), location)) {
            CoordinatesManager.insertOrReplace(getApplicationContext(), coordinates);
            TripApplication.setLocation(location);
        }
        List<Coordinates> list = CoordinatesManager.loadAll(getApplicationContext());
        Toast.makeText(getApplicationContext(), mCurrentLocation.getLatitude() + "" + mCurrentLocation.getLongitude(), Toast.LENGTH_SHORT);

        Log.d("tag", mCurrentLocation.toString());
        stopService(new Intent(getBaseContext(), LocationService.class));
        // onDestroy();
        //  updateUI();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("LocalService", "Received start id " + startId + ": " + intent);
        mGoogleApiClient.connect();
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        mGoogleApiClient.disconnect();
        getApplicationContext().startService(new Intent(getApplicationContext(), LocationServices.class));
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        createLocationRequest();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(this, "Disconnected. Please re-connect.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
    }
}
