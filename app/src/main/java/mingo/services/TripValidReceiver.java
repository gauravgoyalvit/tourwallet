package mingo.services;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.recording.models.NewTrip;

import mingo.managers.NewTripManager;
import mingo.util.Utilities;

public class TripValidReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        // For our recurring task, we'll just display a message
        Toast.makeText(context, "I'm running", Toast.LENGTH_SHORT).show();
        Long id = intent.getLongExtra("localId", 0);
        Log.d("tag", "tagging");

        NewTrip newTrip_ = NewTripManager.isAnyTripRunningAlready(context);

        if(newTrip_==null) {
            Utilities.startingSetup(context,id);
        }
        else {
            Utilities.showAlreadyExistingPopup(context,id);
        }
    }


}