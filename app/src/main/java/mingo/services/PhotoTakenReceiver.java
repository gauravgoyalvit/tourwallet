package mingo.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.widget.Toast;

import mingo.TripApplication;
import mingo.jobs.PhotoAddJob;
import mingo.managers.NotificationMessageManager;

public class PhotoTakenReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        abortBroadcast();
        Log.d("New Photo Clicked", ">");
        Cursor cursor = context.getContentResolver().query(
                intent.getData(), null, null, null, null);
        cursor.moveToFirst();
        String image_path = cursor
                .getString(cursor.getColumnIndex("_data"));
        // this is to update current location, so that photo can be categorized well.
        context.startService(new Intent(context, LocationService.class));

        // Adding photo related info to db
        TripApplication.getInstance().getJobManager()
                                    .addJob(new PhotoAddJob(image_path));

        Toast.makeText(context, "New Photo is Saved as : " + image_path,
                1000).show();

        NotificationMessageManager.showTripRecordNotification(context);
    }
}