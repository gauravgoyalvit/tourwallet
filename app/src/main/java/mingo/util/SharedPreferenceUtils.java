package mingo.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Sunil on 07-Oct-15.
 */
public class SharedPreferenceUtils {

    // instance of shared preference
    private static SharedPreferenceUtils sInstance;

    private SharedPreferences mPrefs;

    // getting the instance of shared preference
    public static SharedPreferenceUtils getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new SharedPreferenceUtils(context);
        }
        return sInstance;
    }

    private SharedPreferenceUtils(Context context) {
        mPrefs = context.getSharedPreferences(Constants.SHAREDPREFERENCE, 0);
    }

    public void setUserImage(String value) {

        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(Constants.USER_IMAGE, value);
        editor.commit();

    }

    public String getUserImage() {
        return mPrefs.getString(Constants.USER_IMAGE, null);
    }

    public void setUserEmail(String email) {

        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(Constants.USER_EMAIL, email);
        editor.commit();

    }

    public String getUserEmail() {
        return mPrefs.getString(Constants.USER_EMAIL, null);
    }

    public void setUserName(String name) {

        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(Constants.USER_NAME, name);
        editor.commit();

    }

    public String getUserName() {
        return mPrefs.getString(Constants.USER_NAME, null);
    }

    public void setToekn(String token) {

        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(Constants.TOKEN, token);
        editor.commit();

    }

    public String getToken() {
        return mPrefs.getString(Constants.TOKEN, null);
    }


}
