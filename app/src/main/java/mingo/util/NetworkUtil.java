package mingo.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

/**
 * Created by Sunil on 07-Oct-15.
 */
public class NetworkUtil {

    public static boolean isNetworkAvailable(Context ctx) {
        ConnectivityManager connectivityManager = null;
        NetworkInfo activeNetworkInfo;
        try
        {
            connectivityManager = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
            activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo != null && activeNetworkInfo.isAvailable() && activeNetworkInfo.isConnected()) {
                return true;
            } else {
                Toast.makeText(ctx,"Network Error", Toast.LENGTH_SHORT).show();
                return false;
            }
        } catch(NullPointerException e){
            Toast.makeText(ctx, "Internet Error", Toast.LENGTH_SHORT).show();
            return false;
        }
    }
}
