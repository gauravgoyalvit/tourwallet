package mingo.util;

import android.content.Context;

import com.recording.models.NewTrip;
import com.recording.models.Pics;

import java.util.Calendar;

import mingo.managers.NewTripManager;
import mingo.managers.PicsManager;

/**
 * Created by gaurav on 04/10/15.
 */
public final class Constants {

    private Constants() {
    }

    // 30 mins

    // production mode
//    public static final int AlarmTriggerInterval = 1000 * 60 * 30;
//
//    public static final long locationServiceFastInterval = 1000 * 60;
//
//
//    public static final long locationServiceInterval = 1000 * 60 * 30;
//
//
//    public static final float minimumDistaceForInsertingDataToDB = 750.00f;

    //debug mode

    public static final String SHAREDPREFERENCE = "MyPrefs";
    public static final String USER_IMAGE ="UserImage";
    public static final String USER_NAME ="UserName";
    public static final String USER_EMAIL ="UserEmail";
    public static final String SETTING_UP_PROFILE = "Setting your profile";
    public static final String TOKEN ="token";

    // 30 sec

    public static final int AlarmTriggerInterval = 1000 * 30; // 30 sec

    // 10 sec
    public static final long locationServiceFastInterval = 1000 * 10; // 10 sec

    // 1 min
    public static final long locationServiceInterval = 1000 * 60; // 1 min

    public static final float minimumDistaceForInsertingDataToDB = 0.00f;


    public static void setPics(Context context){
        Pics pic1 = new Pics();
        NewTrip trip = NewTripManager.isAnyTripRunningAlready(context);
        pic1.setNewTrip(trip);
        pic1.setAdminarea("karnatka"); // karmatka
        pic1.setCountryname("india");
        pic1.setLocality("kormangla");// bangalore
        pic1.setPicname("/storage/emulated/0/DCIM/Camera/IMG_20151110_232132.jpg");
        pic1.setPictripid(1L);
        pic1.setPremises("1");
        pic1.setSubadminarea("11"); // null or bangalore
        pic1.setSublocality("12");
        pic1.setSubthoroughfare("13");
        pic1.setThoroughfare("14");

        pic1.setPhotoDate(Calendar.getInstance().getTime());
        PicsManager.insertOrReplace(context, pic1);

        Pics pic3 = new Pics();
        pic3.setNewTrip(trip);
        pic3.setAdminarea(null); // karmatka
        pic3.setCountryname("india");
        pic3.setLocality("kormangla");// bangalore
        pic3.setPicname("/storage/emulated/0/DCIM/Camera/IMG_20151110_232132.jpg");
        pic3.setPictripid(1L);
        pic3.setPremises("1");
        pic3.setSubadminarea("11"); // null or bangalore
        pic3.setSublocality("12");
        pic3.setSubthoroughfare("13");
        pic3.setThoroughfare("14");

        pic3.setPhotoDate(Calendar.getInstance().getTime());
        PicsManager.insertOrReplace(context, pic3);

        Pics pic2 = new Pics();
        pic2.setNewTrip(trip);
        pic2.setAdminarea("karnatka"); // karmatka
        pic2.setCountryname("german");
        pic2.setLocality("kormangla");// bangalore
        pic2.setPicname("/storage/emulated/0/DCIM/Camera/IMG_20151007_223936.jpg");
        pic2.setPictripid(1L);
        pic2.setPremises("1");
        pic2.setSubadminarea("11"); // null or bangalore
        pic2.setSublocality("12");
        pic2.setSubthoroughfare("13");
        pic2.setThoroughfare("14");
        pic2.setPhotoDate(Calendar.getInstance().getTime());
        //PicsManager.insertOrReplace(context, pic2);


    }




    //debug mode



}
