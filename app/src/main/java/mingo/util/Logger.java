package mingo.util;

import android.support.v4.BuildConfig;
import android.util.Log;

/**
 * Created by Ashish Prasad on 10/2/15.
 */
public final class Logger {

    private Logger() {
    }

    public static void i(String tagName, String description) {
        if (BuildConfig.DEBUG) {
            Log.i(tagName, description);
        }
    }

    public static void d(String tagName, String description) {
        if (BuildConfig.DEBUG) {
            Log.d(tagName, description);
        }
    }


    public static void e(String tagName, String description) {
        if (BuildConfig.DEBUG) {
            Log.e(tagName, description);
            Log.e(tagName, Thread.currentThread().getStackTrace().toString());
        }
    }

}
