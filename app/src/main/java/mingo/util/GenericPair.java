package mingo.util;

/**
 * Created by Ashish Prasad on 10/10/15.
 */
public class GenericPair<U, V> {

    /**
     * The first element of this <code>Pair</code>
     */
    private U first;

    /**
     * The second element of this <code>Pair</code>
     */
    private V second;

    /**
     * Constructs a new <code>Pair</code> with the given values.
     *
     * @param first  the first element
     * @param second the second element
     */
    public GenericPair(U first, V second) {

        this.first = first;
        this.second = second;
    }

    //getter for first and second
    public U getFirst() {
        return first;
    }

    public void setFirst(U first) {
        this.first = first;
    }

    public V getSecond() {
        return second;
    }

    public void setSecond(V second) {
        this.second = second;
    }
}

