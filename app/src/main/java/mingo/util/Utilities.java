package mingo.util;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import com.recording.models.NewTrip;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import mingo.TripApplication;
import mingo.managers.NewTripManager;
import mingo.services.AlarmReceiver;
import mingo.services.DeviceBootReceiver;
import mingo.services.PhotoTakenReceiver;

/**
 * Created by gaurav on 04/10/15.
 */
public final class Utilities {

    private Utilities() {
    }

    public static Boolean isLocationValid(Context context, Location location) {
        Location lastLocation = ((TripApplication) context.getApplicationContext()).getLocation();
        float dist;
        double earthRadius = 6371000; //meters
        if (lastLocation != null) {
            Double lat1 = lastLocation.getLatitude();
            Double lng1 = lastLocation.getLongitude();
            Double lat2 = location.getLatitude();
            Double lng2 = location.getLongitude();
            double dLat = Math.toRadians(lat2 - lat1);
            double dLng = Math.toRadians(lng2 - lng1);
            double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                    Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                            Math.sin(dLng / 2) * Math.sin(dLng / 2);
            double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            dist = (float) (earthRadius * c);
        } else {
            dist = Constants.minimumDistaceForInsertingDataToDB;
        }

        if (dist >= Constants.minimumDistaceForInsertingDataToDB)
            return true;
        else return false;
    }

    public static void showAlert(Activity activity, String message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(message);

        builder.setCancelable(false);
        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();

            }

        });

        AlertDialog alert = builder.create();
        alert.show();
        TextView messageText = (TextView) alert.findViewById(android.R.id.message);
        messageText.setGravity(Gravity.CENTER);
    }

    public static void showAlreadyExistingPopup(final Context context,final Long id){
        final NewTrip newTrip = NewTripManager.isAnyTripRunningAlready(context);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                TripApplication.getCurrentActivity());
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder
        .setTitle("Already Running Trip :" + newTrip.getName())
                .setMessage("You have already Scheduled Trip " + newTrip.getName() + " Are you sure you want to stop this trip?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // update trip flag and update end time
                        endTrip(newTrip,context);
                        // start coordinate service for new trip
                        startingSetup(context,id);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.cancel();
                        Toast.makeText(context,"You can start this trip manually later.",Toast.LENGTH_SHORT);
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public static void endTrip(NewTrip newTrip,Context context){
        Calendar newCalendar = Calendar.getInstance();
        Date date = newCalendar.getTime();
        newTrip.setTripon(false);
        newTrip.setEndtime(date);
        NewTripManager.insertOrReplace(context, newTrip);

        // ending alarm manager
        AlarmManager alarmManager = TripApplication.getAlarmManager();
        PendingIntent pendingIntent = TripApplication.getPendingIntent();
        if(alarmManager!=null   )
        alarmManager.cancel(pendingIntent);

        disableDeviceReceiver(context);
        disablePhotoReceiver(context);
    }

    public static void startingSetup(Context context,Long id){
        // enabling new trip
        NewTrip newTrip = NewTripManager.findByLocalId(context, id);
        newTrip.setTripon(true);
        NewTripManager.insertOrReplace(context, newTrip);
        Toast.makeText(context, "Your Trip has been started", Toast.LENGTH_SHORT).show();

        // setting alarm manager
        Intent alarmIntent = new Intent(context, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);
        AlarmManager manager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
        int interval = Constants.AlarmTriggerInterval;

        manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
        Toast.makeText(context, "Alarm Set", Toast.LENGTH_SHORT).show();
        TripApplication.setPendingIntent(pendingIntent);
        TripApplication.setAlarmManager(manager);

        enableDeviceRecieverReceiver(context);
        enablePhotoRecieverReceiver(context);
    }


    // for new trips
    public static void showAlreadyExistingPopup(final Context context,final NewTrip trip){
        final NewTrip newTrip = NewTripManager.isAnyTripRunningAlready(context);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                TripApplication.getCurrentActivity());
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder
                .setTitle("Already Running Trip :" + newTrip.getName())
                .setMessage("You have already Running Trip " + newTrip.getName() + " Are you sure you want to stop this trip?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // update trip flag and update end time
                        endTrip(newTrip, context);
                        // start coordinate service for new trip
                        startingSetup(context,trip);
                        TripApplication.getInstance().getCurrentActivity().finish();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.cancel();
                        Toast.makeText(context,"You can start this trip manually later.",Toast.LENGTH_SHORT);
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public static void startingSetup(Context context,NewTrip newTrip){
        // enabling new trip
        newTrip.setTripon(true);
        NewTripManager.insertOrReplace(context, newTrip);
        Toast.makeText(context, "Your Trip has been started", Toast.LENGTH_SHORT).show();

        // setting alarm manager
        Intent alarmIntent = new Intent(context, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);
        AlarmManager manager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
        int interval = Constants.AlarmTriggerInterval;

        manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
        Toast.makeText(context, "Alarm Set", Toast.LENGTH_SHORT).show();
        TripApplication.setPendingIntent(pendingIntent);
        TripApplication.setAlarmManager(manager);

        enableDeviceRecieverReceiver(context);
        enablePhotoRecieverReceiver(context);
    }

    public static void enablePhotoRecieverReceiver(Context context)
    {
        ComponentName receiver = new ComponentName(context, PhotoTakenReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }
    /**
     * This method disables the Broadcast receiver registered in the AndroidManifest file.
     */
    public static void disablePhotoReceiver(Context context){
        ComponentName receiver = new ComponentName(context, PhotoTakenReceiver.class);
        PackageManager pm = context.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);

    }

    public static void enableDeviceRecieverReceiver(Context context)
    {
        ComponentName receiver = new ComponentName(context, DeviceBootReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }
    /**
     * This method disables the Broadcast receiver registered in the AndroidManifest file.
     */
    public static void disableDeviceReceiver(Context context){
        ComponentName receiver = new ComponentName(context, DeviceBootReceiver.class);
        PackageManager pm = context.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);

    }



}
